const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req,res) => {
    res.send('nothing to see here.');
})

app.post('/update-user', (req, res) => {
    out = JSON.stringify(req);
    res.send(`post received on /update user: ${out}`);
})

app.get('/get-user', (req, res) => {
    console.log(req);
    let user_id = req.query.user;
    res.send(`have a user: ${user_id}`);
})

app.listen(port, () => {
    console.log(`SE app listening on port ${port}`);
})